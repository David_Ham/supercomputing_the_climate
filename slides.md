class: center

# Supercomputing the&nbsp;weather and climate

## David Ham
### David.Ham@imperial.ac.uk

---
class: center

# Supercomputing the&nbsp;weather and climate

## or

# What on Earth does computer&nbsp;science have to do with the planet?

---

<video src="images/cm24_sst_1920x1080_avidemux_mpg4.ogv" controls></video>

Sea surface temperature in the National Oceanic and Atmospheric Administration Geophysical Fluid Dynamics Laboratory's climate model.

---
class: center, middle

![Theory Experiment Simulation](images/threelegs.svg)

## Simulation: the third leg of science.

---

# First, we need some theory

.center[![Vilhem Bjerknes](images/bjerknes.jpg)]

In 1904 Vilhem Bjerknes
proposes a set of differential equations relating wind (in three
dimensions), pressure, temperature and moisture at __every__ point in
the atmosphere. He proposed some graphical methods for solving them:
but that's not what interests us.

---

# From theory to algorithm

.center[![Lewis Fry Richardson](images/Lewis_Fry_Richardson.png)]

During  the second decade of the twentieth century, Lewis Fry Richardson came up with the idea of weather forecasting by discretising the equations.

---

.scale[![Richardson's grid](images/richardsongrid.jpg)]

---

# Next we need some computers...

.center[![Computers at work](images/human_computers2.jpg)]

Computers at work on the mathematical tables project (1938-1948)
Image from David Alan Grier
Human computers: the first pioneers of the information age
Endeavour, Volume 25, Issue 1, 1 March 2001, Pages 28–32

---

# Really, quite a lot of computers...

.scale60.center[![Richardson's Forecast Factory](images/forecastfactory.jpg)]

Richardson's concept of a "forecast factory", artist's impression by F. Schuiten.

---

# Fortunately, technology intervened

.scale60.center[![The ENIAC Computer](images/ENIAC.jpg)]

ENIAC, one of the first electronic computers. 

---

## One of the early calculations performed was weather...

.scale50.center[![ENIAC Computations team](images/ENIAC-group.png)]

Left to right: Harry Wexler, John von Neumann, M. H. Frankel, Jerome Namias, John Freeman, Ragnar Fjörtoft, Francis Reichelderfer and Jule Charney

Charney, Fjörtoft and von Neumann, 1950: 
Numerical Integration of the Barotropic Vorticity Equation
Tellus, 2, 237-254.

---

## In 2008, the same calculation was performed on a mobile Phone!


.scale30.center[![Phoniac forecast](images/NOKIA-FORECAST.jpg)]

The implementation of the J2ME program phoniac.jar on a cell phone is described in 
Peter Lynch & Owen Lynch, 2008: Forecasts by PHONIAC. Weather, 63, 324-326

---

### Today, weather still ties up many of the largest computers.

.tiny[<table class="table table-condensed">
<thead>
    <tr>

    <th>World Rank</th>
        <th>Site</th>
        <th>System</th>
        <th>Cores</th>
        <th>Rmax (TFlop/s)</th>
        <th>Rpeak (TFlop/s)</th>
        <th>Power (kW)</th>
    </tr>
</thead>
            
<tr class="sublist odd">
                    
    <td><span class="badge">19</span></td>
    <td><a href="/site/47752">ECMWF</a><br>United Kingdom</td>
    <td><a href="/system/178431">Cray XC30, Intel Xeon E5-2697v2 12C 2.7GHz, Aries interconnect</a><br/>Cray Inc.</td>
    <td>83,160</td>
    <td>1,552.0</td>
    <td>1,796.3</td>
    <td></td>
</tr>

<tr class="sublist even">
                    
<td><span class="badge">20</span></td>
<td><a href="/site/47752">ECMWF</a><br>United Kingdom</td>
<td><a href="/system/178430">Cray XC30, Intel Xeon E5-2697v2 12C 2.7GHz, Aries interconnect 
</a><br/>Cray Inc.</td>
                    <td>83,160</td>
                
                     <td>1,552.0</td>
                    <td>1,796.3</td>
                
                    <td></td>
                </tr>
            
                <tr class="sublist odd">
                    
                    <td><span class="badge">23</span></td>
                    <td><a href="/site/47670">Science and Technology Facilities Council - Daresbury Laboratory</a><br>United Kingdom</td>
                    <td><a href="/system/177723">
                        <b>Blue Joule</b> - BlueGene/Q, Power BQC 16C 1.60GHz, Custom
                    </a><br/>IBM</td>
                    <td>131,072</td>
                
                     <td>1,431.1</td>
                    <td>1,677.7</td>
                
                    <td>657</td>
                </tr>
            
                <tr class="sublist even">
                    
                    <td><span class="badge">25</span></td>
                    <td><a href="/site/50487">EPSRC/University of Edinburgh</a><br>United Kingdom</td>
                    <td><a href="/system/178188">
                        <b>ARCHER</b> - Cray XC30, Intel Xeon E5 v2 12C 2.700GHz, Aries interconnect 
                    </a><br/>Cray Inc.</td>
                    <td>76,192</td>
                
                     <td>1,367.5</td>
                    <td>1,645.7</td>
                
                    <td></td>
                </tr>
            
                <tr class="sublist odd">
                    
                    <td><span class="badge">33</span></td>
                    <td><a href="/site/49151">University of Edinburgh</a><br>United Kingdom</td>
                    <td><a href="/system/177724">
                        <b>DiRAC</b> - BlueGene/Q, Power BQC 16C 1.60GHz, Custom
                    </a><br/>IBM</td>
                    <td>98,304</td>
                
                     <td>1,073.3</td>
                    <td>1,258.3</td>
                
                    <td>493.1</td>
                </tr>
            
                <tr class="sublist even">
                    
                    <td><span class="badge">40</span></td>
                    <td><a href="/site/47363">AWE</a><br>United Kingdom</td>
                    <td><a href="/system/178421">
                        <b>Spruce A</b> - SGI ICE X, Intel Xeon E5-2680v2  10C 2.8GHz, Infiniband FDR
                    </a><br/>SGI</td>
                    <td>44,520</td>
                
                     <td>958.7</td>
                    <td>997.2</td>
                
                    <td>855.1</td>
                </tr>
            
                <tr class="sublist odd">
                    
                    <td><span class="badge">49</span></td>
                    <td><a href="/site/47363">AWE</a><br>United Kingdom</td>
                    <td><a href="/system/178422">
                        <b>Spruce B</b> - SGI ICE X, Intel Xeon E5-2680v2  10C 2.8GHz, Infiniband FDR
                    </a><br/>SGI</td>
                    <td>35,640</td>
                
                     <td>767.5</td>
                    <td>798.3</td>
                
                    <td>684.5</td>
                </tr>
            
                <tr class="sublist even">
                    
                    <td><span class="badge">59</span></td>
                    <td><a href="/site/49151">University of Edinburgh</a><br>United Kingdom</td>
                    <td><a href="/system/177231">
                        <b>HECToR</b> - Cray XE6, Opteron 6276 16C 2.30 GHz, Cray Gemini interconnect
                    </a><br/>Cray Inc.</td>
                    <td>90,112</td>
                
                     <td>660.2</td>
                    <td>829.0</td>
                
                    <td></td>
                </tr>
            
                <tr class="sublist odd">
                    
                    <td><span class="badge">60</span></td>
                    <td><a href="/site/47752">ECMWF</a><br>United Kingdom</td>
                    <td><a href="/system/177564">Power 775, POWER7 8C 3.836GHz, Custom Interconnect
                    </a><br/>IBM</td>
                    <td>24,576</td>
                
                     <td>635.1</td>
                    <td>754.2</td>
                
                    <td>1,386.9</td>
                </tr>
            
                <tr class="sublist even">
                    
                    <td><span class="badge">61</span></td>
                    <td><a href="/site/47752">ECMWF</a><br>United Kingdom</td>
                    <td><a href="/system/177563">Power 775, POWER7 8C 3.836GHz, Custom Interconnect
                    </a><br/>IBM</td>
                    <td>24,576</td>
                
                     <td>635.1</td>
                    <td>754.2</td>
                
                    <td>1,386.9</td>
                </tr>
            
                <tr class="sublist odd">
                    
                    <td><span class="badge">78</span></td>
                    <td><a href="/site/49064">United Kingdom Meteorological Office</a><br>United Kingdom</td>
                    <td><a href="/system/177561">Power 775, POWER7 8C 3.836GHz, Custom Interconnect
                    </a><br/>IBM</td>
                    <td>18,432</td>
                
                     <td>476.3</td>
                    <td>565.6</td>
                
                    <td>1,040.2</td>
                </tr>
            
                <tr class="sublist even">
                    
                    <td><span class="badge">97</span></td>
                    <td><a href="/site/49064">United Kingdom Meteorological Office</a><br>United Kingdom</td>
                    <td><a href="/system/177562">Power 775, POWER7 8C 3.836GHz, Custom Interconnect
                    </a><br/>IBM</td>
                    <td>15,360</td>
                
                     <td>396.9</td>
                    <td>471.4</td>
                
                    <td>866.8</td>
                </tr>
</table>]

The top 12 machines in the UK in June 2014. Source: http://top500.org

---

### it's traditional at this point to show a picture of a supercomputer

.center.scale80[![Unified model performance](images/Cray_ECMWF.jpg)]

ECMWF's new Cray supercomputer, currently the largest in the UK.

---

## and actually the UK is pretty good at this...

.center.scale80[![Unified model performance](images/foo.png)]

Northern hemisphere 3 day pressure errors for a variety of international weather models.

---

## but we still need vastly more resolution...

.center[![Climate resolution](images/mesh_resolutions.gif)]

Image courtesy of the Intergovernmental Panel on Climate Change 4th Assessment Report

---

## However the computational challenges are fierce.

.center.scale60[![Parallel scaling of the Unified model](images/scaling.png)]

Parallel scaling of the UK Met Office's Unified Model at 17km resolution.

---

## In parallel, processors have to cooporate.

.center.scale60[![Domain decomposition of sphere](images/domain_decomposition.jpg)]

Parallel domain decomposition of a sphere. Image from A.A Mirin, D.E Shumaker, M.F Wehner
Efficient filtering techniques for finite-difference atmospheric general circulation models on parallel processors1
Parallel Computing, Volume 24, Issues 5–6, June 1998, Pages 729–740

---

## The Earth has these inconvenient poles

.center.scale50[![Met office mesh with converging poles](images/metoffice_mesh.png)]

At 25km resolution over the UK, the polar resolution is 75m! At 10km resolution it would be 10m!



---

## Part of the answer is nesting

.center.video60[<video src="images/katrina-rh-draft5.ogv" controls></video>]

---

## But the long term requires new meshes

.center.scale[![Strange meshes](images/spheremesh.png)]

Meshes with less structure

---

## Which makes the parallel performance problem far harder

.center[![ADCIRC Decomposition](images/unstructured_domain.jpg)]

Domain decomposition for an unstructured mesh of the Gulf of Mexico

Image from J.C. Dietrich, M. Zijlema, J.J. Westerink, L.H. Holthuijsen, C. Dawson, R.A. Luettich Jr., R.E. Jensen, J.M. Smith, G.S. Stelling, G.W. Stone
Modeling hurricane waves and storm surge using integrally-coupled, scalable computations Coastal Engineering, Volume 58, Issue 1, January 2011, Pages 45–65

---

## Conclusion

The weather and climate are some of the toughest but most important problems in computing.

Computer scientists, can contribute to this in many ways:

* New parallel algorithms.
* More efficient data structures.
* Grid specialised compilers.
* New programming languages for science.
* ...

If you can cross the discipline boundary with maths (eg with JMC):

* New more accurate and stable numerical methods.
* Mathematics-aware compilers and data structures
* ...
